#include "configuration.h"

using namespace std;

/* Class constructor. */
CConfiguration::CConfiguration( vector<vector<int>> matrix, int l3Cnt, int l4Cnt, int id, int emptyTiles, int impossibleTiles ) {
    for (int i = 0; i < matrix.size(); i++){
        // construct a vector of int
        vector<int> v;
        for (int j = 0; j < matrix[i].size(); j++)
            v.push_back(matrix[i][j]);

        this->field.push_back(v);
    }

    this->l3Cnt = l3Cnt;
    this->l4Cnt = l4Cnt;
    this->emptyTiles = emptyTiles;
    this->imposibleTiles = impossibleTiles;
    this->id = id;
}

int CConfiguration::bufferSize() {
    return (static_cast<int>(field.size()));
}

void CConfiguration::fillBufferToSend( int * buffer ){
    buffer[5] = this->l3Cnt;
    buffer[6] = this->l4Cnt;
    buffer[7] = this->emptyTiles;
    buffer[8] = this->imposibleTiles;
    buffer[9] = this->id;
    int k = 10;
    for(int i = 0; i < field.size(); i++){
        for( int j = 0; j < field[i].size(); j++ ) {
            buffer[k] = field[i][j];
            k++;
        }
    }
}

void CConfiguration::loadDataFromBuffer( int *buffer, int N, int M ) {
    this->l3Cnt = buffer[3];
    this->l4Cnt = buffer[2];
    this->emptyTiles = buffer[1];
    this->imposibleTiles = 0;

    int k = 4;
    for (int i = 0; i < N; i++){
        // construct a vector of int
        vector<int> v;
        for (int j = 0; j < M; j++) {
            v.push_back(buffer[k]);
        k++;
        }

        this->field.push_back(v);
    }
}

vector<vector<int>> CConfiguration::getField() {
    return this->field;
}

/* Print operator */
ostream & operator<<(ostream & os, const CConfiguration & obj){
    for( vector<int> row : obj.field){
        for( int value : row ){
            os << setw(2) << value << " ";
        }
        os << endl;
    }
    return os;
}